FROM registry.gitlab.com/cunity/linux-cmake-builders/ubuntu-18.04:master
ADD build/tinish /bin/
ADD tini/build/tini-static /bin/tini
ADD install.sh /
RUN sh /install.sh
RUN sh -c "env|sort"
