# Tinish

*NOTE* This won't work if your /bin/sh uses argv0 to work out what to do (eg busybox).

Sometimes you really want your linux container to run an init process but have no say
in how the first process is executed.

One example of this is running a build or test using `gitlab-runner` with kubernetes.

Gitlab starts the container by setting the pod `Command`, which has the effect of not executing the `ENTRYPOINT` defined in your image at all.

What `tinish` does, is this:

* If we are not PID 1 we exec `ARGV[0].real ARGS`
* If we *ARE* PID 1 we exec `tini -- ARGV[0].real ARGS` instead

# License

Tinish is released under the MIT license (see `LICENSE`)
